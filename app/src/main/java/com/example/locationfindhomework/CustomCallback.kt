package com.example.locationfindhomework

interface CustomCallback {
    fun onResponse(response: String)
    fun onFailure(response: String)
}