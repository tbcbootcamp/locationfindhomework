package com.example.locationfindhomework

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    private lateinit var locationRecyclerViewAdapter: LocationRecyclerViewAdapter
    private var addresses = mutableListOf<LocationModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        progressBar.visibility = View.GONE
        locationRecyclerViewAdapter = LocationRecyclerViewAdapter(addresses)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = locationRecyclerViewAdapter
        searchEditText.addTextChangedListener(textWatcher)
    }

    private fun getAddresses(input: String) {

        clear(input)
        progressBar.visibility = View.VISIBLE

        val parameters = mutableMapOf<String, String>()
        parameters["input"] = input
        parameters["key"] = "AIzaSyBukqO846TNjXqU_Mc_kAFFaC3Wdt-PFaU"

        ApiHandler.getRequest(ApiHandler.AUTOCOMPLETE, parameters, object : CustomCallback {

            override fun onFailure(response: String) {
                Toast.makeText(applicationContext, "failure", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(response: String) {

                clear(input)

                val json = JSONObject(response)

                if (json.has("predictions")) {

                    val predictions = json.getJSONArray("predictions")

                    (0 until predictions.length()).forEach {
                        val prediction = predictions.getJSONObject(it)
                        addresses.add(
                                LocationModel(
                                        prediction.getString("description"),
                                        prediction.getString("place_id")
                                )
                        )
                    }
                }

                locationRecyclerViewAdapter.notifyDataSetChanged()
                progressBar.visibility = View.GONE
            }
        })
    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            getAddresses(s.toString())
        }

    }

    private fun clear(input: String) {
        if (input.isEmpty()) {
            addresses.clear()
            locationRecyclerViewAdapter.notifyDataSetChanged()
            progressBar.visibility = View.GONE
            return
        }
    }
}